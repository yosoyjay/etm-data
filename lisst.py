""" Methods to handle LISST data 
"""
#--------------------------------------------------------------------------------
# Imports  
#--------------------------------------------------------------------------------
import numpy as np

#--------------------------------------------------------------------------------
# LISST constants 
#--------------------------------------------------------------------------------
# Type C instrument used on cruise - 2.5 to 500 microns
lisst_vol_sizes = np.array([2.72, 3.20, 3.78, 4.46, 5.27, 6.21, 7.33, 8.65,
                            10.2, 12.1, 14.2, 16.8, 19.8, 23.4, 27.6, 32.5,
                            38.4, 45.3, 53.5, 63.1, 74.5, 87.9, 104,  122,
                            144,  170,  201,  237,  280,  331,  390,  460])

lisst_vol_names = np.array(['2.72 um', '3.20 um', '3.78 um', '4.46 um', 
                            '5.27 um', '6.21 um', '7.33 um', '8.65 um',
                            '10.2 um', '12.1 um', '14.2 um', '16.8 um', 
                            '19.8 um', '23.4 um', '27.6 um', '32.5 um',
                            '38.4 um', '45.3 um', '53.5 um', '63.1 um', 
                            '74.5 um', '87.9 um', '104 um',  '122 um',
                            '144 um',  '170 um',  '201 um',  '237 um',  
                            '280 um',  '331 um',  '390 um',  '460 um',
                            'elev'])

lisst_units = dict((k,'ul/l') for k in lisst_vol_names)
lisst_units['elev'] = 'db'  

lisst_diams_names = ['total_volume', 'mean_size', 'std', 'nan', 
                     'D10', 'D16', 'D50', 'D60', 'D84',
                     'D90', 'hazen_uniformity_coefficient',
                     'surface_area', 'silt_density',
                     'silt_volume']

lisst_diams_units = ['ul/l', 'um', 'um', 'NaN', 
                     'um',   'um', 'um', 'um', 'um',
                     'um', 'ratio', 'cm2/l', 'percent', 
                     'um']

lisst_units_2 = dict((k,v) for k,v in zip(lisst_diams_names, lisst_diams_units))
lisst_units.update(lisst_units_2)
