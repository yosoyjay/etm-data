""" Class and methods to deal with Owen's Tube data from Spring and Fall ETM 
cruises.
"""
#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------
import csv
import os 
import datetime
import unittest

#-------------------------------------------------------------------------------
# Functions 
#-------------------------------------------------------------------------------
class OwensTube(object):
	""" Read and format data from Owen's Tube csv files from CMOP ETM cruises """
	def __init__(self, path):
		""" 
		Parameters
		----------
			* path - Path to csv files
		"""
		if os.path.exists(path):
			self.path = path
		else:
			raise Exception('File not found: '+path)


	def fetchAndFormat(self):
		"""Read and returns the Owen's Tube data from the spreadsheets provided by
		Lindy Fine from UM-Horne Point as a dictionary with the following format

		owens_tube[sample#] - Value is dictionary of data
		data['variable'] - Value is value associated with variable for that sample#

		E.G. - owens_tube['1354']['Time'] - Will give the time of sample 1354
		"""
		data1 = fetchData(self.path)
		data2 = formatData(data1)
		data3 = formatTime(data2)
		self.data = data3

	def fetchData(self):
		""" Read Owen's Tube data form CSV sheets. 
		"""
		f = self.path
		data = []
		with open(f, 'U') as file:
			reader = csv.reader(file, delimiter=',')
			for row in reader:
				data.append(row)

		return data	

	def formatData(self, data):
		"""Formats data return readOwensTubeData into a reasonable format.  
			 A dictionary of Sample # and values for each parameter.


		Parameters
		----------
			* data - Data returned from fetchOwensTube
		"""
		# Make keys from headers
		keys = []
		# Special cases, need details from 2nd line (data[1])
		special = ['BP', 'O2 Resp', 'DIC', 'Nutrients', 'Pigments']
		for i in range(len(data[0])):
			if data[0][i] in special:
				keys.append(data[0][i] + ' : ' + data[1][i])
			elif data[0][i] == '':
				key_base = keys[-1][:keys[-1].find(' :')]
				keys.append(key_base + ' : ' + data[1][i])
			else:
				keys.append(data[0][i])

		ot = {}
		# Make dir with (sampleIDs,var) as key  
		for sample in range(2,len(data)):
			ot[data[sample][0]] = {}
			for var in range(1,len(keys)):
				ot[data[sample][0]][keys[var]] = data[sample][var]

		return ot

	def formatTime(self, data):
		"""Formats Owen's Tube Time to UTC from (PDT) and converts it to a Timestamp
		in UTC [Hardcoded +7 changing PDT TO UTC]] to match the times from Winched
		profiler.

		Parameters
		----------
			* data - Formatted Data returned from fetchData

		"""
		for sample in data.keys():
			if sample == '': continue
			t = data[sample]['Time']
			d = data[sample]['Date']
			time = datetime.datetime.strptime(d + ' ' + t, '%d-%b-%y %H:%M') 
			# Add 7 hours to time for PDT to UTC for both ETM (Fall and Spring 2012) cruises.
			# timedelta(day, seconds)
			time = time + datetime.timedelta(0, 7*3600)
			data[sample]['Time'] = time 

		return data
