"""
Holds the many constansts used in processing the ETM cruise data that vary
based on cruise.

Jesse Lopez - 01/29/2013
"""
#-------------------------------------------------------------------------------
# Imports 
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Load constants 
#-------------------------------------------------------------------------------
def load_constants(cruise):
  """Loads constants used for ETM data processing which vary over cruises.

  This is also a convenient place to hold much of the processing details.
  instruments
  date_shape
  days

  Parameters:
  -----------
    cruise - Name of cruise ['spring', 'fall_site_1', 'fall_site_2']
  """
  cruises = ['spring', 'fall_site_1', 'fall_site_2']
  if cruise not in cruises:
    print 'Cruise name not recognized: '+cruise
    raise
  if cruise == 'spring':
    n_days = 7
    instruments = ['CTD', 'ALTIM', 'COMP', 'MOPAK', 'VG1', 'VG2', 'ACC1', 'ACC2', 
                   'uT', 'uC', 'OXY', 'ADV', 'ECO', 'LISST']
    # Times are in UTC 
    # A set of files from the first deployment day are excluded because they
    # it seems like many of them do not have a lot of data 
    day1 = [datetime.datetime(2012, 5, 1, 15, 20, 00),
            datetime.datetime(2012, 5, 1, 16, 25, 00)] 
    day2 = [datetime.datetime(2012, 5, 2, 18, 30, 00),
            datetime.datetime(2012, 5, 2, 23, 50, 00)] 
    day3 = [datetime.datetime(2012, 5, 3, 15, 40, 00),
            datetime.datetime(2012, 5, 4, 04, 55, 00)] 
    day4 = [datetime.datetime(2012, 5, 4, 14, 00, 00),
            datetime.datetime(2012, 5, 5, 15, 50, 00)] 
    day5 = [datetime.datetime(2012, 5, 5, 14, 15, 00),
            datetime.datetime(2012, 5, 6, 05, 55, 00)] 
    day6 = [datetime.datetime(2012, 5, 6, 15, 00, 00),
            datetime.datetime(2012, 5, 7, 8, 00, 00)] 
    day7 = [datetime.datetime(2012, 5, 7, 16, 00, 00),
            datetime.datetime(2012, 5, 8, 03, 55, 00)] 
    days = [eval('day%d' % d) for d in range(1,n_days+1)]
    anchor = '46 14.0 N, 123 53.4 W'
  elif cruise == 'fall_site_1':
    n_days = 4
    instruments = ['CTD', 'ALTIM', 'COMP', 'MOPAK', 'VG1', 'VG2', 'ACC1', 'ACC2', 
                   'uT', 'uC', 'OXY', 'ADV', 'PUCK', 'FLNTU', 'LISST']
    # Times are in UTC 
    day1 = [datetime.datetime(2012, 10, 26, 16, 30, 00),
            datetime.datetime(2012, 10, 26, 23, 55, 00)]
    day2 = [datetime.datetime(2012, 10, 27, 00, 00, 00),
            datetime.datetime(2012, 10, 27, 23, 55, 00)]
    day3 = [datetime.datetime(2012, 10, 28, 00, 00, 00),
            datetime.datetime(2012, 10, 28, 23, 55, 00)]
    day4 = [datetime.datetime(2012, 10, 29, 00, 00, 00),
            datetime.datetime(2012, 10, 29, 07, 50, 00)]
    days = [eval('day%d' % d) for d in range(1,n_days+1)]
    anchor = '46 14.0 N, 123 53.4 W'
  else: 
    n_days = 3

    instruments = ['CTD', 'ALTIM', 'COMP', 'MOPAK', 'VG1', 'VG2', 'ACC1', 'ACC2', 
                   'uT', 'uC', 'OXY', 'ADV', 'PUCK', 'FLNTU', 'LISST']
    day1 = [datetime.datetime(2012, 10, 29, 07, 00, 00),
           datetime.datetime(2012, 10, 29, 23, 55, 00)]
    day2 = [datetime.datetime(2012, 10, 30, 00, 00, 00),
           datetime.datetime(2012, 10, 30, 23, 55, 00)]
    day3 = [datetime.datetime(2012, 10, 31, 00, 00, 00),
           datetime.datetime(2012, 10, 31, 23, 55, 00)]
    days = [eval('day%d' % d) for d in range(1,n_days+1)]
    anchor = '46 14.0 N, 123 53.4 W'


  lisstVars = ['Unknown %i' % x for x in range(1,41)]
  vars = {'CTD': ['temp', 'conductivity', 'Pressure', 
                  'Salinity'],
          'ALTIM': ['temp', 'range'],
          'COMP': ['heading 1', 'heading 2', 'pitch', 'roll', 
                   'unknown 1', 'unknown 2'],
          'MOPAK': ['unkown 1', 'tilt 1', 'tilt2', 'tilt3', 'angular rate 1',
                    'angular rate 2', 'angular rate 3'],
          'VG1': ['VG1'],
          'VG2': ['VG2'],
          'ACC1': ['ACC1'],
          'ACC2': ['ACC2'],
          'uT': ['uT'],
          'uC': ['uC'],
          'OXY': ['o2'],
          'ADV': ['unknown 1', 'Velocity 1', 'Velocity 2', 
                  'Velocity 3', 'Amplitude 1',
                  'Amplitude 2', 'Amplitude 3', 
                  'cor1', 'cor2', 'cor3'],
          'ECO': ['unknown 1', 'Red', 'unknown 3', 'Blue', 
                  'Chlorophyll', 'unknown 6'],
          'PUCK': ['unknown 1', 'Red', 'unknown 3', 'Blue', 
                  'Chlorophyll', 'unknown 6'],
          'LISST': lisstVars,
          'FLNTU': ['Lambda1', 'chlorophyll', 'Lambda2', 'NTU', 'Thermister']
         }

  lisstUnits = ['?' for x in range(1,41)]
  units = {'CTD': ['C', '?', 'dbar', 'psu'],
           'ALTIM': ['C', 'm'],
           'COMP': ['degrees', 'degrees', 'degrees', 'degrees', '?', '?'],
           'MOPAK': ['?', '?', '?', '?', '?', '?', '?'],
           'VG1': ['?'],
           'VG2': ['?'],
           'ACC1': ['?'],
           'ACC2': ['?'],
           'uT': ['?'],
           'uC': ['?'],
           'OXY': ['?'],
           'ADV': ['?', 'mm/s', 'mm/s', 'mm/s', 'db/0.43', 'db/0.43', 'db/0.43',
                   '?', '?', '?'],
           'ECO': ['?', '?', '?', '?', '?', '?'],
           'PUCK': ['?', '?', '?', '?', '?', '?'],
           'FLNTU': ['?', '?', '?', '?', '?'],
           'LISST': lisstUnits,
          }

  dataType = {'CTD': 'f4',
              'ALTIM': 'f4',
              'COMP': 'f4',
              'MOPAK': 'i4',
              'VG1': 'i4',
              'VG2': 'i4',
              'ACC1': 'i4',
              'ACC2': 'i4',
              'uT': 'i4',
              'uC': 'i4',
              'OXY': 'i4',
              'ADV': 'f4',
              'ECO': 'f4',
              'PUCK': 'f4',
              'FLNTU': 'f4',
              'LISST': 'i4',
             }

  instruSpecs = {'CTD': 'SBE-37',
                 'uT': 'SBE-8', 
                 'uC': 'SBE-7',
                 'o2': 'SBE-43',
                 'ADV': 'SonTek ADV',
                 'LISST': 'LISST 100-X',
                 'ECO PUCK': 'BB2FVMG-154',
                 'Puck': 'BB2FVMG-154',
                 'Motion Pak': 'BEI Motion Pak II',
                 'Altimeter': 'DataSonics PSA 910',
                 'Compass': 'Spartan compass',
                 'FLNTU': 'FLNTU',
                }

# Extra row added to hold time
  dataShapes = {'CTD': (5,1),
                'ALTIM': (3,1),
                'COMP': (7,1),
                'MOPAK': (8,1),
                'VG1': (2,1),
                'VG2': (2,1),
                'ACC1': (2,1),
                'ACC2': (2,1),
                'uT': (2,1),
                'uC': (2,1),
                'OXY': (2,1),
                'ADV': (11,1),
                'ECO': (7,1),
                'PUCK': (7,1),
                'LISST': (41,1),
                'FLNTU': (6,1),
               }

