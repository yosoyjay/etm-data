"""
Holds the many constansts used in processing the ETM cruise data that vary
based on cruise.
"""
#-------------------------------------------------------------------------------
#Imports
#-------------------------------------------------------------------------------
import datetime

#-------------------------------------------------------------------------------
# Time related constants 
#-------------------------------------------------------------------------------
n_days = 3

day1 = [datetime.datetime(2012, 10, 29, 07, 00, 00),
       datetime.datetime(2012, 10, 29, 23, 55, 00)]
day2 = [datetime.datetime(2012, 10, 30, 00, 00, 00),
       datetime.datetime(2012, 10, 30, 23, 55, 00)]
day3 = [datetime.datetime(2012, 10, 31, 00, 00, 00),
       datetime.datetime(2012, 10, 31, 22, 15, 00)]
days = [eval('day%d' % d) for d in range(1,4)]

#-------------------------------------------------------------------------------
# Instrument related constants 
#-------------------------------------------------------------------------------
instruments = ['CTD', 'ALTIM', 'COMP', 'MOPAK', 'VG1', 'VG2', 'ACC1', 'ACC2', 
               'uT', 'uC', 'OXY', 'ADV', 'PUCK', 'FLNTU', 'LISST']

# Change some names to be consistant with CMOP netCDF cache 
inst_name = {'CTD'  : 'CTD',
             'ALTIM': 'Altim',
             'COMP' : 'Comp',
             'MOPAK': 'Mopak',
             'VG1'  : 'VG1',
             'VG2'  : 'VG2',
             'ACC1' : 'ACC1',
             'ACC2' : 'ACC2',
             'uT'   : 'uT',
             'uC'   : 'uC',
             'OXY'  : 'Oxygen',
             'ADV'  : 'ADV',
             'ECO'  : 'Eco',
             'PUCK' : 'Eco',
             'FLNTU': 'FLNTU',
             'LISST': 'LISST',
            }

# Map some names to CMOP netCDF cache standard
# Also, change some names that were fouled when loading into netCDF files.
name = {'temp'           : 'water_temperature',
        'conductivity'   : 'water_conductivity',
        'Pressure'       : 'water_pressure',
        'Salinity'       : 'water_salinity',
        'range'          : 'range',
        'heading 1'      : 'heading1',
        'heading 2'      : 'heading2',
        'pitch'          : 'pitch',
        'roll'           : 'roll',
        'unknown 1'      : 'unknown1',
        'unknown 2'      : 'unknown2',
        'VG1'            : 'VG1',
        'VG2'            : 'VG2',
        'ACC1'           : 'ACC1',
        'ACC2'           : 'ACC2',
        'uT'             : 'uT',
        'uC'             : 'uC',
        'o2'             : 'oxygen',
        'Velocity 1'     : 'vel1',
        'Velocity 2'     : 'vel2',
        'Velocity 3'     : 'vel3',
        'Amplitude 1'    : 'amp1',
        'Amplitude 2'    : 'amp2',
        'Amplitude 3'    : 'amp3',
        'cor1'           : 'cor1',
        'cor2'           : 'cor2',
        'cor3'           : 'cor3',
        'Red'            : 'red',
        'Blue'           : 'blue',
        'Chlorophyll'    : 'cholorophyll',
        'unknown 3'      : 'unknown2',
        'unknown 6'      : 'unknown3',
        'time'           : 'time',
        'angular rate 1' : 'ang_rate_1',
        'angular rate 2' : 'ang_rate_2',
        'angular rate 3' : 'ang_rate_3',
        'tilt 1'         : 'tilt1',
        'tilt2'          : 'tilt2',
        'tilt3'          : 'tilt3',
        'unkown 1'       : 'unknown1',
        'elev'           : 'elev',
        'Lambda1'        : 'lambda1',
        'chlorophyll'    : 'cholorophyll',
        'Lambda2'        : 'lambda2',
        'NTU'            : 'NTU',
        'Thermister'     : 'thermister',
       }

# Reverse dictionsary
rev_names = dict((v,k) for k, v in name.iteritems())

# Standardize units 
units = {'temp'          : 'C',
        'conductivity'   : '?',
        'Pressure'       : 'dbar',
        'Salinity'       : 'psu',
        'range'          : 'm',
        'heading 1'      : 'degrees',
        'heading 2'      : 'degrees',
        'pitch'          : 'degrees',
        'roll'           : 'degrees',
        'unknown 1'      : '?',
        'unknown 2'      : '?',
        'VG1'            : '?',
        'VG2'            : '?',
        'ACC1'           : '?',
        'ACC2'           : '?',
        'uT'             : '?',
        'uC'             : '?',
        'o2'             : '?',
        'Velocity 1'     : 'mm/s',
        'Velocity 2'     : 'mm/s',
        'Velocity 3'     : 'mm/s',
        'Amplitude 1'    : 'dB/0.43',
        'Amplitude 2'    : 'dB/0.43',
        'Amplitude 3'    : 'dB/0.43',
        'cor1'           : '?',
        'cor2'           : '?',
        'cor3'           : '?',
        'Red'            : 'm^-1 sr^-1',
        'Blue'           : 'm^-1 sr^-1',
        'Chlorophyll'    : 'mg/l',
        'unknown 3'      : '?',
        'unknown 6'      : '?',
        'time'           : 'seconds since 1970-01-01 00 : 00 : 00-00',
        'angular rate 1' : '?',
        'angular rate 2' : '?',
        'angular rate 3' : '?',
        'tilt 1'         : '?',
        'tilt2'          : '?',
        'tilt3'          : '?',
        'unkown 1'       : '?',
        'elev'           : 'db',
        'Lambda1'        : '?',
        'chlorophyll'    : 'mg/l',
        'Lambda2'        : '?',
        'NTU'            : 'NTU',
        'Thermister'     : '?',
       }

# LISST vars skipped for now to await later processing because the processed
# data was not saved into the MATLAB files.
vars = {'CTD'   : ['temp', 'conductivity', 'Pressure', 
                   'Salinity'],
        'ALTIM' : ['temp', 'range'],
        'COMP'  : ['heading 1', 'heading 2', 'pitch', 'roll',
                   'unknown 1', 'unknown 2'],
        'MOPAK' : ['unkown 1', 'tilt 1', 'tilt2', 'tilt3', 'angular rate 1',
                   'angular rate 2', 'angular rate 3'],
        'VG1'   : ['VG1'],
        'VG2'   : ['VG2'],
        'ACC1'  : ['ACC1'],
        'ACC2'  : ['ACC2'],
        'uT'    : ['uT'],
        'uC'    : ['uC'],
        'OXY'   : ['o2'],
        'ADV'   : ['unknown 1', 'Velocity 1', 'Velocity 2',
                   'Velocity 3', 'Amplitude 1',
                   'Amplitude 2', 'Amplitude 3', 
                   'cor1', 'cor2', 'cor3'],
        'ECO'   : ['unknown 1', 'Red', 'unknown 3', 'Blue', 
                   'Chlorophyll', 'unknown 6'],
        'PUCK'  : ['unknown 1', 'Red', 'unknown 3', 'Blue', 
                   'Chlorophyll', 'unknown 6'],
        'LISST' : ['Unknown %i' % x for x in range(1,41)],        
				'FLNTU' : ['Lambda1', 'chlorophyll', 'Lambda2', 'NTU', 'Thermister'],
       }

mat_units = {'CTD'   : ['C', '?', 'dbar', 'psu'],
             'ALTIM'    : ['C', 'm'],
             'COMP'     : ['degrees', 'degrees', 'degrees', 'degrees', '?', '?'],
             'MOPAK'    : ['?', '?', '?', '?', '?', '?', '?'],
             'VG1'      : ['?'],
             'VG2'      : ['?'],
             'ACC1'     : ['?'],
             'ACC2'     : ['?'],
             'uT'       : ['?'],
             'uC'       : ['?'],
             'OXY'      : ['?'],
             'ADV'      : ['?', 'mm/s', 'mm/s', 'mm/s', 
							             'db/0.43', 'db/0.43', 'db/0.43',
                           '?', '?', '?'],
             'ECO'      : ['?', '?', '?', '?', '?', '?'],
             'PUCK'     : ['?', '?', '?', '?', '?', '?'],
             'FLNTU'    : ['?', '?', '?', '?', '?'],
             'LISST'    : ['?' for x in range(1,41)],
        }

data_type = {'CTD'   : 'f4',
             'ALTIM' : 'f4',
             'COMP'  : 'f4',
             'MOPAK' : 'i4',
             'VG1'   : 'i4',
             'VG2'   : 'i4',
             'ACC1'  : 'i4',
             'ACC2'  : 'i4',
             'uT'    : 'i4',
             'uC'    : 'i4',
             'OXY'   : 'i4',
             'ADV'   : 'f4',
             'ECO'   : 'f4',
             'PUCK'  : 'f4',
             'FLNTU' : 'f4',
             'LISST' : 'i4',
           }


inst_specs = {'CTD'        : 'SBE-37',
              'uT'         : 'SBE-8',
              'uC'         : 'SBE-7',
              'o2'         : 'SBE-43',
              'ADV'        : 'SonTek ADV',
              'LISST'      : 'LISST 100-X',
              'ECO PUCK'   : 'BB2FVMG-154',
              'Puck'       : 'BB2FVMG-154',
              'Motion Pak' : 'BEI Motion Pak II',
              'Altimeter'  : 'DataSonics PSA 910',
              'Compass'    : 'Spartan compass',
              'FLNTU'      : 'FLNTU',
             }
 
# Extra row added to hold time
data_shapes = {'CTD'   : (5,1),
               'ALTIM' : (3,1),
               'COMP'  : (7,1),
               'MOPAK' : (8,1),
               'VG1'   : (2,1),
               'VG2'   : (2,1),
               'ACC1'  : (2,1),
               'ACC2'  : (2,1),
               'uT'    : (2,1),
               'uC'    : (2,1),
               'OXY'   : (2,1),
               'ADV'   : (11,1),
               'ECO'   : (7,1),
               'PUCK'  : (7,1),
               'LISST' : (41,1),
               'FLNTU' : (6,1),
              }

#-------------------------------------------------------------------------------
# Ship constants 
#-------------------------------------------------------------------------------
# oc1204 location - Bounces a bit around here over the cruise.
# There is a sift slightly west on decimal day 123, but still quite close
lon = -123.915
lat = 46.233
