"""Miscellaneous functions used while exploring and processing data.
"""
#-------------------------------------------------------------------------------
# Interp functions
#-------------------------------------------------------------------------------
import os
import netCDF4
import numpy as np
import seawater.csiro as sw  
import processing

#-------------------------------------------------------------------------------
# Interp functions
#-------------------------------------------------------------------------------
def interpVars(dfData1, var1, dfData2, var2):
  """ Interpolates data of var1 onto the time of var2 using simple
  linear interpolation. 

  Parmeters
  ---------
    dfData1 -- DataFrame object that contains var1
    var1 -- Key into DataFrame of data for variable
    dfData2 -- DataFrame object that contains var2
    var2 -- Key into DataFrame of data for variable

  Returns
  -------
    tsData -- Series object with interpolated data 
  """
  #idx = idTimeBounds(dfData1, dfData1) 
  # Hand coded for development of ADV data for first day
  # Adjust to use indentified indices
  f = interp1d(dfData1['time'][:], dfData1[var1][:])
  tsData = f(dfData2['time'][1:]) 

  return tsData

def interpDFtoCTD(ctd, df):
  """Interpolates all data in DataFrame object to CTD time.
  """
  for k in df.keys():
    f = interp1d(df['time'][:], df[k][:])
    df[k][:] = f(ctd['time'][1:])

  return df

#-------------------------------------------------------------------------------
# Other calculation functions
#-------------------------------------------------------------------------------
def calcBuoyFreq(ctd, rho='potential'):
  """Calculates the buoyancy erequency (Brunt-Vaisala) using CTD data frame.

  Parameters
  ----------
    ctd - CTD DataFrame object
    rho - 'potential' = rho_theta or 'average' uses mean of rho 

  Returns
  -------
    N - Buoyancy frequency  

  Notes
  -----
  rho:
    'potential':
      .. math::
      N = \sqrt{-\frac{g}{\rho_theta}\frac{d\rho_theta}{dz}}
      rho_theta - Potential density calculated via seawater package
    'average':
      .. math::
      N = \sqrt{-\frac{g}{\rho_mean}\frac{d\rho}{dz}}
      rho_mean - Density average over file
  
  References
  ----------
    Petrie, J, et. al (2010). Local boundary shear stress estimates from velocity 
      profiles with an ADCP. River Flow 2010. ISBN 978-3-939230-00-7

    Mikkelsen, O, et. al (2008). The influence of schlieren on in situ optical
      measurements for particle characterization. Limnology and Oceanography:
      Methods. Vol. 6. 133-143.
  """
  if rho=='potential':
    rho_theta = sw.pden(ctd.water_salinity, ctd.water_temperature, ctd.water_pressure)
    drho = rho_theta.diff()
    dz = ctd.water_pressure.diff()
    g = -9.8
    N = np.sqrt(np.abs(-g/rho_theta*drho/dz))
    return N 
  elif rho=='average':
    rho = sw.dens(ctd.water_salinity, ctd.water_temperature, ctd.water_pressure)
    rho_mean = np.mean(rho)
    drho = rho.diff()
    dz = ctd.water_pressure.diff()
    g = -9.8
    N = np.sqrt(-g/rho_mean*drho/dz)
    return N 
  else:
    print 'Method not recognized'
    raise

def wrap(ctd, bins):
  makeLisstMask(ctd, bins)

def makeLisstMask(ctd, bins, threshold=0.025):
  """Creates boolean array to act as a mask for LISST and other data.
  
  Parmeters
  ---------
    ctd - CTD DataFrame
    bins - Array-like bins specs 
    threshold - N threshold: $N = \sqrt{-\frac{g}{\rho}*\frac{\partial \rho}{\partial z}}$

  Returns
  -------
    bin_good - List of indexers into data for each salinity class defined in bins.  Last
               entry is all good data.
  """
  # Ignore all data with bf > threshold and nans and infs
  bf = calcBuoyFreq(ctd)
  good = bf <= threshold 
  good = np.logical_and(good, np.logical_not(np.isnan(good)))
  good = np.logical_and(good, np.logical_not(np.isinf(good)))

  # Split data into salinity classes and ignore bad data
  binned = np.digitize(ctd.water_salinity, bins) 
  bin_good = []
  for b in range(len(bins)):
    bin_idx = binned == b
    bin_idx = np.logical_and(bin_idx, good)
    bin_good.append(bin_idx)
  bin_good.append(good)

  return bin_good 

def maskMCD(x, y, threshold=5):
  """Creates a mask of good data based on the MCD < threshold

  Parameters
  ----------
    x - Array like 
    y - Array like
    threshold - > threshold is bad data.

  Returns
  -------
    mask - Indexer of x, y with data of distance < threshold
  """
  X = np.transpose(np.vstack((x, y)))
  MCD = MinCovDet().fix(X)
  d = MCD.mahalanobis(X)
  
  return d < threshold

def calcPercentGoodLisst(bf, criteria=0.025):
  """Calculates the percent of good LISST data based on N (buoyancy frequency)
  """
  bad_value = bf > criteria
  bad_nan = np.isnan(bf)
  bad = np.logical_or(bad_value, bad_nan)
  return 1 - float(np.sum(bad))/bf.size

def addDepth(df, ctd):
  """Returns df with depth added based on pressure measurement interpolated to 
  same time as instrument.
  """
  # Fresh water correction for decibar to meters from Sea-Bird
  depth = ctd['Pressure']*1.019716
  df['elev'] = depth.values[:] 

  return df

def smoothAltim(alt, window, percent):
  """ Remove spikes from data based on percent of average of last n values

  This method is totatally based on heruistics, but effective with following
  Parameters:
    window = 3
    percent = 20

  1. Apply median filter of size 3
  2. Remove spikes defined as > percent of last 5 points mean and replace
     with average of last 5
  3. Go through one more time and remove any spikes using same policy
  4. Smooth using size 5 moving average
  """
  alt = sp.signal.medfilt(alt,3) 
  end = window/2
  for i in np.arange(end, alt.shape[0]-(end+1)):
    avg = np.mean(alt[i-end:i+end+1])
    #print i, alt[i], avg
    if np.abs(alt[i]-avg)/avg > percent*0.01:  
      print 'Removing value %d and replacing with average' % i
      alt[i] = avg 

  for i in np.arange(0, alt.shape[0]-1):
    if np.abs(alt[i] - alt[i+1])/alt[i] > percent*0.01:
      print 'Removing value %d' % i
      alt[i] = avg 

  alt = cbs.smooth(alt, window_len=5, window='flat')
  return alt

def calcDepth(altimDF, ctdDF): 
  """ Calculates the depth of the watercolumn using altimetery data and the
  pressure from the CTD.
  """
  sAltim = smoothAltim(altimDF, 3, 20)
  f = interp1d(altimDF['time'][::6], sAltim[::6])
  sAltim = f(ctdDF['time'])
  depth = sAltim + ctdDF['Pressure'][:]
  
  return depth

def idContiguousIndices(dfIndex):
  """ Identifies contiguous sets of indices.

  Parameters
  ----------
    dfIndex - df.index

  returns
  -------
    indexSet - List of tuples with beginning and ending index of contiguous
  """
  indexSet = []
  start = dfIndex[0]
  last = dfIndex[0] 
  end = dfIndex[-1]
  for idx in range(1, dfIndex.shape[0]-1):
    if (dfIndex[idx+1] - dfIndex[idx]) > datetime.timedelta(0, 60):
      print 'Making set ', start, last
      indexSet.append([start, last])
      start = dfIndex[idx]
      last = dfIndex[idx]
    else:
      last = dfIndex[idx]
      #print idx, dfIndex[idx], start, last

  if last == end:
    print 'Making set ', start, last
    indexSet.append([start, last])

  if start == dfIndex[0]:
    print 'A single contiguous set'
    indexSet.append([dfIndex[0], dfIndex[-1]])

  return indexSet

def calcOutOfWater(ctdDF):
  """ Identifies time ranges when the profiler is out of the water.  

  Consider any pressure < 0.1 to be out of water to provide buffer for bad
  data. 

  OOW - Out of water
a
  Parameters
  ----------
    ctdDF - DataFrame of ctd instrument data

  returns
  -------
    times - List of times of when the profiler is out of the water. 
  """
  OOWidx = ctdDF['Pressure'][ctdDF['Pressure'] < 0.05 ] 
  OOWsets = idContiguousIndices(OOWidx.index)
  times = []
  for set in OOWsets:
    times.append([ctdDF['time'][set[0]], ctdDF['time'][set[1]]])
  return times

def filterOOW(times, df):
  """ Changes values from instruments to np.nan for times identified as being
  out of the water [Changes are inplace].

  Note:
  Some DataFrame objects are typecast to float64 because int arrays cannot hold
  np.nan.

  Parameters
  ----------
    times - List of start and end times
    df - DataFrame of instrument data

  returns
    df - DataFrame of instrument data with values removed
  """
  df = df.apply(np.float64)
  for t in times:
    low = df['time'] > t[0]                    
    high = df['time'] < t[1]
    idx = df['time'][low & high]
    # Do not change time to np.nan
    timeIdx = np.where(df.columns == 'time')[0][0]
    print 'Removing %d values when the WP was out of the water' % idx.shape
    df.ix[idx.index,:timeIdx] = np.nan
    df.ix[idx.index,(timeIdx+1):] = np.nan

  return df

def idTimeBounds(dfData1, dfData2) :
  """ Identifies indices into var1 to begin interpolation
  to avoid any extrapolation.
  """
  idx = dfData2['time'] > dfData1['time']  
  # Find first True
  # Find last time in dfData2 < dfData1
  # return these two indices

def applyEulerRotation(euler) :
  """ Applies Euler rotation to ADV velocity data as described by eulerAngles as
  implemented by John Dunlap in cmwp_src codes.

  Parameters
  ----------
    adv - DataFrame of adv data
    euler- An ndarray (3,1) of Euler angles to apply to adv data

  returns
  -------
    r - Rotation matrix
  """
  # Rotation about x-axis 3rd
  R1 = np.array([[1, 0, 0],
                 [0,  np.cos(euler[0]), np.sin(euler[0])],
                 [0, -np.sin(euler[0]), np.cos(euler[0])]])
  # Rotation about y-axis 2nd
  R2 = np.array([[np.cos(euler[1]), 0, -np.sin(euler[1])],
                 [0,  1,                0],
                 [np.sin(euler[1]), 0, np.cos(euler[1])]])
  # Rotation about z-axis 1st
  R3 = np.array([[np.cos(euler[2]), np.sin(euler[2]), 0],
                 [-np.sin(euler[2]), np.cos(euler[2]), 0],
                 [0,  0,                1]])

  R = R1 * R2 * R3

  return R, R1, R2, R3

def medFilter(a):
  """ Calculates mean of readings before and after, if current is greater than
  or less than mean+10% then it is replaced with mean.
  """
  b = a.copy
  for i in xrange(b.shape[0]):
    m = (b[i-1] + b[1+1])/2.0
    if np.abs(b[i] - m)/m > 0.1:
      b[i] = m
  return b

def calcVerticalBin(df):
  """Calculates the size of the vertical "bin," or the vertical distanced 
  traveled by the winched profiler between measurements.

  Charles recommended a "bin" size of 20cm.

  Parameters
  ----------
    df - DataFrame object of WP data

  Returns
  -------
  """
  dz = df.pressure.diff()
  # Returns microseconds, so change it to seconds 
  dt = df.index.values.diff()
  dt = dt/1e9
  return dz/dt
