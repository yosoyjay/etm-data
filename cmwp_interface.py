#!/usr/bin/python
"""
Collection of classes and methods to fetch, store, and manipulate CMWP 
(winched profiler) data.

Some data about instruments from cmwpshow.m, cmwp2mat.c, and cruise docs.
Instruments:
  CTD         : SBE-37
  uT          : SBE-8
  uC          : SBE-7
  KE dissipation :
  o2          : SBE-43
  ADV         : SonTek ADV
  LISST       : LISST-100X
  ECO PUCK    : BB2FVMG-154, calibration values in cmwpshow.m 
	              (cruise plan sez flor and obs)
                - According to the manual obs can be calculated 
  Motion Pak  : BEI Motion Pak II 
  Altimeter   : DataSonics PSA-910
  Compass     : Spartan 303 magnetic compass

Jesse Lopez 01/02/2013
"""
#-------------------------------------------------------------------------------
# Imports 
#-------------------------------------------------------------------------------
import os
import sys
import datetime
import scipy.io
import netCDF4
import numpy as np
from optparse import OptionParser

#-------------------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------
def calc_file_times(day) :
  """Helper function to create list of files for a single deployment date.
  """
  base_time = consts.days[day][0]
  iters = (consts.days[day][1] - consts.days[day][0]).seconds/300
  times = [base_time + datetime.timedelta(minutes=5*x) for x in range(0,iters+1)]

  return times

def lisst_mat_to_nc(matlab_dir):
  """Aggregates data in original MATLAB files from the cmwp into deployment day 
  files saved as NetCDF files for each instrument.

  Dates and division of files are hardcoded.
  """
  for day in range(len(consts.days)):
    times = calc_file_times(day)
    # Create zeros for ndarray and then dump them after loading in the data
    # No way to know in advance of array size
    vol = np.zeros((1,32)) 
    diams = np.zeros((1,14))
    zscat = np.zeros((1,40))
    time_data = np.zeros((1,1))
    for time in times:
      mat_file = 'lisst_cmwp-%s.mat' % (time.strftime('%Y%m%d-%H%M%S'))
      mat_file = os.path.join(matlab_dir, mat_file)
      # Check for bad files to skip over
      if not os.path.isfile(mat_file): 
        continue
      print 'Opening '+mat_file
      data = scipy.io.loadmat(mat_file, mat_dtype=True)
      data = data['out_data'][0,0]
      vol = np.vstack((vol, data[0]))
      diams = np.vstack((diams, data[1]))
      time_data = np.vstack((time_data, np.transpose(data[3])))
      zscat = data[2]
    vol = vol[1:,:]
    diams = diams[1:,:]
    time_data = time_data[1:,:]

    # Write NetCDF files
    ncFile = 'cmwp_day%d_LISST_PROC.nc' % (day+1)
    print 'Creating netCDF file '+ncFile
    f = netCDF4.Dataset(ncFile, 'w', format='NETCDF4', zlib=True)
    f.createDimension('time', time_data.shape[0])
    f.createDimension('vol', 32) 
    f.createDimension('diams', 14)
    f.createDimension('zscat', 40)
    # Volume
    tmpVar = f.createVariable('vol', 'f8', ('time','vol',))
    tmpVar[:,:] = vol
    # Diameters
    tmpVar = f.createVariable('diams', 'f8', ('time','diams',))
    tmpVar[:,:] = diams
    # Z-Scatter?
    tmpVar = f.createVariable('zscat', 'f8', ('zscat',))
    tmpVar[:] = zscat
    # Time
    tmpVar = f.createVariable('time', 'f8', ('time',))
    tmpVar[:] = time_data
    tmpVar.units = 'UTC'
    f.close()

def cmwp_mat_to_nc(matlab_dir) :
  """Aggregates data in original MATLAB files from the cmwp into deployment day 
  files saved as NetCDF files for each instrument.

  Dates and division of files are hardcoded.

  Writes one instrument at a time for memory consumption purposes.
  """
  # cmwp-yyyymmdd-hhmmss.mat
  for inst in instruments :
    print 'Transferring '+inst
    for day in range(len(consts.days)) :
      tmp = np.zeros(data_shapes[inst])
      times = calc_file_times(day)
      #times = [datetime.datetime(2012, 5, 1, 15, 20, 00)]
      for time in times :
        mat_file = 'cmwp-%s.mat' % (time.strftime('%Y%m%d-%H%M%S'))
        mat_file = os.path.join(matlab_dir, mat_file)
        print 'Opening '+mat_file
        # Need to load Matlab file dtype because some data is cast wrong
        data = scipy.io.loadmat(mat_file, mat_dtype=True)
        time_key = 'mlt_'+inst
        tmp = np.concatenate((tmp, 
                              np.concatenate((data[inst], data[time_key]))),
                              axis=1)

      # Write NetCDF files
      tmp = tmp[:,1:] 
      ncFile = 'cmwp_day%d_%s.nc' % (day+1, inst)
      print 'Creating netCDF file '+ncFile
      f = netCDF4.Dataset(ncFile, 'w', format='NETCDF4', zlib=True)
      for i,v in enumerate(vars[inst]):
        f.createDimension(v, tmp.shape[1])
        tmpVar = f.createVariable(v, consts.data_type[inst], (v,))
        tmpVar[:] = tmp[i,:]
        tmpVar.units = consts.mat_units[inst][i]
      f.createDimension('time', tmp.shape[1])
      tmpVar = f.createVariable('time', 'f8', ('time',))
      tmpVar[:] = tmp[-1,:]
      tmpVar.units = 'UTC'
      f.close()

#-------------------------------------------------------------------------------
# Main 
#-------------------------------------------------------------------------------
if __name__ == "__main__":
	cruises = ['spring', 'fall1', 'fall2']
	usage = "Usage: %prog raw_matlab_dir etm_cruise ['spring', 'fall1', 'fall2']"
	parser = OptionParser(usage=usage)
	parser.add_option("-l", action="store_true", help="Process LISST data",
			              default_value=False, dest=lisst, type="bool")
	(options, args) = parser.parse_args()
	if len(args) != 2:
		parser.error("Incorrect number of args")
	if arg[1] not in cruises:
		parser.error("That's not a valid ETM cruise.")
	elif arg[1] == 'spring':
		import etm.spring_consts as consts
	elif arg[1] == 'fall1':
		import etm.fall_1_consts as consts
	else arg[1] == 'fall2':
		import etm.fall_2_consts as consts

	if lisst:
		lisst_mat_to_nc(arg[0])
	else:
		cmwp_mat_to_nc(arg[0])	
