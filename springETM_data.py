#/usr/local/bin/python
"""
Scratch as I manipulate and play with Spring ETM data.

Jesse Lopez, 01/10/2013
"""

#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------
import sys
sys.path.append('../src/')
import etmDataInterface as etm
import cookb_signalsmooth as cbs
from scipy.interpolate import interp1d

#-------------------------------------------------------------------------------
# Consts 
#-------------------------------------------------------------------------------
ctdFile = 'cmwp_day2_CTD.nc'
altimFile = 'cmwp_day2_ALTIM.nc'

#-------------------------------------------------------------------------------
# Commands
#-------------------------------------------------------------------------------

# Calculate depth of water column
ctd = etm.loadNetCDF(ctdFile)				# DF
altim = etm.loadNetCDF(altimFile)			# DF
depth = etm.calcDepth(altim, ctd)			# DF a bit noisy, smooth it
depth_s1 = cbs.smooth(depth.values, window_len=100, window='hanning')

# Compare depth of WP from pressure and from (depth_s1 - altim)
f = interp1d(altim['time'][:], altim['range']) 
altimIONctd = f(ctd['time'][:])

# The pressure sensor looks better than the heavily smoothed and interpolated
# altim data
