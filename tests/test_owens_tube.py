""" Test OwensTube Class and methods """

import unittest
from owens_tube import OwensTube

class testOwensTube(unittest.TestCase):
	""" Test class for OwensTube class methods """

	def setUp(self):
		path = 'tests/owens_tube_tests.csv'
		self.ot = OwensTube(path)

	def testFetchData(self):
		nRows = 4
		nCols = 44 
		d = self.ot.fetchData()
		self.assertEqual(len(d), nRows)
		self.assertEqual(len(d[0]), nCols)

	def testFormatData(self):
		keys = ['1359', '1354']
		nCols = 43
		d = self.ot.fetchData()
		d2 = self.ot.formatData(d)
		self.assertEqual(d2.keys(), keys)
		self.assertEqual(len(d2[keys[0]]), nCols)
		self.assertEqual(len(d2[keys[1]]), nCols)

	def testFormatTime(self):
		import datetime
		k = ['1354', '1359']
		t = {}
		t[k[0]] = datetime.datetime(2012, 5, 1, 21, 43)
		t[k[1]] = datetime.datetime(2012, 5, 3, 16, 34)
		d = self.ot.fetchData()
		d2 = self.ot.formatData(d)
		d3 = self.ot.formatTime(d2)
		self.assertEqual(d3[k[0]]['Time'], t[k[0]])
		self.assertEqual(d3[k[1]]['Time'], t[k[1]])

def suite():
	suite = unittest.TestSuite()
	suite.addTest(unittest.makeSuite(testOwensTube))

	return suite

if __name__ == '__main__':
	suiteFew = unittest.TestSuite()
	suiteFew.addTest(testOwensTube('testFetchData'))
	suiteFew.addTest(testOwensTube('testFormatData'))
	suiteFew.addTest(testOwensTube('testFormatTime'))
	unittest.TextTestRunner(verbosity=2).run(suite())
