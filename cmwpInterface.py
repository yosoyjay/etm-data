#!/usr/bin/python
"""
Collection of classes and methods to fetch, store, and manipulate CMWP 
(winched profiler) data.

Some data about instruments from cmwpshow.m, cmwp2mat.c, and cruise docs.
Instruments:
  CTD         : SBE-37
  uT          : SBE-8
  uC          : SBE-7
  KE dissipation :
  o2          : SBE-43
  ADV         : SonTek ADV
  LISST       : LISST-100X
  ECO PUCK    : BB2FVMG-154, calibration values in cmwpshow.m (cruise plan sez flor and obs)
                - According to the manual obs can be calculated 
  Motion Pak  : BEI Motion Pak II 
  Altimeter   : DataSonics PSA-910
  Compass     : Spartan 303 magnetic compass

lopezj 01/02/2013
"""
#-------------------------------------------------------------------------------
# Imports 
#-------------------------------------------------------------------------------
import numpy as np
import os
import sys
import datetime
import scipy.io
import netCDF4

#-------------------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------------------
#instruments = ['CTD', 'ALTIM', 'COMP', 'MOPAK', 'VG1', 'VG2', 'ACC1', 'ACC2', 
#               'uT', 'uC', 'OXY', 'ADV', 'ECO', 'LISST']
# Fall
instruments = ['CTD', 'ALTIM', 'COMP', 'MOPAK', 'VG1', 'VG2', 'ACC1', 'ACC2', 
               'uT', 'uC', 'OXY', 'ADV', 'LISST', 'FLNTU', 'PUCK']

lisstVars = ['Unknown %i' % x for x in range(1,41)]
vars = {'CTD': ['temp', 'conductivity', 'Pressure', 
                'Salinity'],
        'ALTIM': ['temp', 'range'],
        'COMP': ['heading 1', 'heading 2', 'pitch', 'roll', 
                 'unknown 1', 'unknown 2'],
        'MOPAK': ['unkown 1', 'tilt 1', 'tilt2', 'tilt3', 'angular rate 1',
                  'angular rate 2', 'angular rate 3'],
        'VG1': ['VG1'],
        'VG2': ['VG2'],
        'ACC1': ['ACC1'],
        'ACC2': ['ACC2'],
        'uT': ['uT'],
        'uC': ['uC'],
        'OXY': ['o2'],
        'ADV': ['unknown 1', 'Velocity 1', 'Velocity 2', 
                'Velocity 3', 'Amplitude 1',
                'Amplitude 2', 'Amplitude 3', 
                'cor1', 'cor2', 'cor3'],
        'ECO': ['unknown 1', 'Red', 'unknown 3', 'Blue', 
                'Chlorophyll', 'unknown 6'],
        'PUCK': ['unknown 1', 'Red', 'unknown 3', 'Blue', 
                'Chlorophyll', 'unknown 6'],
        'LISST': lisstVars,
        'FLNTU': ['Lambda1', 'chlorophyll', 'Lambda2', 'NTU', 'Thermister']
       }

lisstUnits = ['?' for x in range(1,41)]
units = {'CTD': ['C', '?', 'dbar', 'psu'],
         'ALTIM': ['C', 'm'],
         'COMP': ['degrees', 'degrees', 'degrees', 'degrees', '?', '?'],
         'MOPAK': ['?', '?', '?', '?', '?', '?', '?'],
         'VG1': ['?'],
         'VG2': ['?'],
         'ACC1': ['?'],
         'ACC2': ['?'],
         'uT': ['?'],
         'uC': ['?'],
         'OXY': ['?'],
         'ADV': ['?', 'mm/s', 'mm/s', 'mm/s', 'db/0.43', 'db/0.43', 'db/0.43',
                 '?', '?', '?'],
         'ECO': ['?', '?', '?', '?', '?', '?'],
         'PUCK': ['?', '?', '?', '?', '?', '?'],
         'FLNTU': ['?', '?', '?', '?', '?'],
         'LISST': lisstUnits,
        }

dataType = {'CTD': 'f4',
            'ALTIM': 'f4',
            'COMP': 'f4',
            'MOPAK': 'i4',
            'VG1': 'i4',
            'VG2': 'i4',
            'ACC1': 'i4',
            'ACC2': 'i4',
            'uT': 'i4',
            'uC': 'i4',
            'OXY': 'i4',
            'ADV': 'f4',
            'ECO': 'f4',
            'PUCK': 'f4',
            'FLNTU': 'f4',
            'LISST': 'i4',
           }


instruSpecs = {'CTD': 'SBE-37',
               'uT': 'SBE-8', 
               'uC': 'SBE-7',
               'o2': 'SBE-43',
               'ADV': 'SonTek ADV',
               'LISST': 'LISST 100-X',
               'ECO PUCK': 'BB2FVMG-154',
               'Puck': 'BB2FVMG-154',
               'Motion Pak': 'BEI Motion Pak II',
               'Altimeter': 'DataSonics PSA 910',
               'Compass': 'Spartan compass',
              'FLNTU': 'FLNTU',
              }

# Location data
anchor_A = '46 14.0 N, 123 53.4 W'

# Extra row added to hold time
dataShapes = {'CTD': (5,1),
              'ALTIM': (3,1),
              'COMP': (7,1),
              'MOPAK': (8,1),
              'VG1': (2,1),
              'VG2': (2,1),
              'ACC1': (2,1),
              'ACC2': (2,1),
              'uT': (2,1),
              'uC': (2,1),
              'OXY': (2,1),
              'ADV': (11,1),
              'ECO': (7,1),
              'PUCK': (7,1),
              'LISST': (41,1),
              'FLNTU': (6,1),
             }

# Times are in UTC 
# A set of files from the first deployment day are excluded because they
# it seems like many of them do not have a lot of data 
#day1 = [datetime.datetime(2012, 5, 1, 15, 20, 00),
#      datetime.datetime(2012, 5, 1, 16, 25, 00)] 
# A set of files starting with cmwp-20120502-101000.mat are not included.
# The times are not regular.  They will be added manually later. 
#day2 = [datetime.datetime(2012, 5, 2, 18, 30, 00),
#      datetime.datetime(2012, 5, 2, 23, 50, 00)] 
#day3 = [datetime.datetime(2012, 5, 3, 15, 40, 00),
#      datetime.datetime(2012, 5, 4, 04, 55, 00)] 
#day4 = [datetime.datetime(2012, 5, 4, 14, 00, 00),
#      datetime.datetime(2012, 5, 5, 15, 50, 00)] 
#day5 = [datetime.datetime(2012, 5, 5, 14, 15, 00),
#      datetime.datetime(2012, 5, 6, 05, 55, 00)] 
#day6 = [datetime.datetime(2012, 5, 6, 15, 00, 00),
#      datetime.datetime(2012, 5, 7, 8, 00, 00)] 
#day7 = [datetime.datetime(2012, 5, 7, 16, 00, 00),
#      datetime.datetime(2012, 5, 8, 03, 55, 00)] 
#days = [eval('day%d' % d) for d in range(1,8)]

# Fall cruise - Site 1 
#day1 = [datetime.datetime(2012, 10, 26, 16, 30, 00),
#day1 = [datetime.datetime(2012, 10, 26, 17, 40, 00),
#        datetime.datetime(2012, 10, 26, 23, 55, 00)]
#day2 = [datetime.datetime(2012, 10, 27, 00, 00, 00),
#        datetime.datetime(2012, 10, 27, 23, 55, 00)]
#day3 = [datetime.datetime(2012, 10, 28, 00, 00, 00),
#        datetime.datetime(2012, 10, 28, 23, 55, 00)]
#day4 = [datetime.datetime(2012, 10, 29, 00, 00, 00),
#        datetime.datetime(2012, 10, 29, 07, 50, 00)]
#days = [eval('day%d' % d) for d in range(1,5)]

# Fall cruise - Site 2
day1 = [datetime.datetime(2012, 10, 29, 07, 00, 00),
       datetime.datetime(2012, 10, 29, 23, 55, 00)]
day2 = [datetime.datetime(2012, 10, 30, 00, 00, 00),
       datetime.datetime(2012, 10, 30, 23, 55, 00)]
day3 = [datetime.datetime(2012, 10, 31, 00, 00, 00),
       datetime.datetime(2012, 10, 31, 22, 15, 00)]
days = [eval('day%d' % d) for d in range(1,4)]

# LISST Times - Different than the others because there were more empty
# files.
# There is no file for the first day, so there is a dummy file there
# for the function. 
#day1 = [datetime.datetime(2012, 5, 2, 17, 10, 00),
#        datetime.datetime(2012, 5, 2, 17, 10, 00)] 
#day2 = [datetime.datetime(2012, 5, 2, 18, 30, 00),
#        datetime.datetime(2012, 5, 2, 23, 50, 00)] 
#day3 = [datetime.datetime(2012, 5, 3, 15, 40, 00),
#        datetime.datetime(2012, 5, 4, 04, 55, 00)] 
#day4 = [datetime.datetime(2012, 5, 4, 14, 00, 00),
#        datetime.datetime(2012, 5, 5, 15, 50, 00)] 
#day5 = [datetime.datetime(2012, 5, 5, 14, 15, 00),
#        datetime.datetime(2012, 5, 6, 05, 55, 00)] 
#day6 = [datetime.datetime(2012, 5, 6, 15, 00, 00),
#        datetime.datetime(2012, 5, 7, 8, 00, 00)] 
#day7 = [datetime.datetime(2012, 5, 7, 16, 05, 00),
#        datetime.datetime(2012, 5, 8, 03, 55, 00)] 
#days = [eval('day%d' % d) for d in range(1,8)]


#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------

def calcFileTimes(day) :
  """Helper function to create list of files for a single deployment date.
  """
  baseTime = days[day][0]
  iters = (days[day][1] - days[day][0]).seconds/300
  times = [baseTime + datetime.timedelta(minutes=5*x) for x in range(0,iters+1)]

  return times

def lisstMatToNc(matlabDir):
  """Aggregates data in original MATLAB files from the cmwp into deployment day 
  files saved as NetCDF files for each instrument.

  Dates and division of files are hardcoded.
  """
  for day in range(len(days)):
    times = calcFileTimes(day)
    # Create zeros for ndarray and then dump them
    # No way to know in advance of array size
    vol = np.zeros((1,32)) 
    diams = np.zeros((1,14))
    zscat = np.zeros((1,40))
    time_data = np.zeros((1,1))
    for time in times:
      matFile = 'lisst_cmwp-%s.mat' % (time.strftime('%Y%m%d-%H%M%S'))
      matFile = os.path.join(matlabDir, matFile)
      # Check for bad files to skip over
      if not os.path.isfile(matFile): 
        continue
      print 'Opening '+matFile
      data = scipy.io.loadmat(matFile, mat_dtype=True)
      data = data['out_data'][0,0]
#      data = data['out_file'][0,0]
      vol = np.vstack((vol, data[0]))
      diams = np.vstack((diams, data[1]))
      time_data = np.vstack((time_data, np.transpose(data[3])))
      zscat = data[2]
    vol = vol[1:,:]
    diams = diams[1:,:]
    time_data = time_data[1:,:]

    # Write NetCDF files
    ncFile = 'cmwp_day%d_LISST_PROC.nc' % (day+1)
    print 'Creating netCDF file '+ncFile
    f = netCDF4.Dataset(ncFile, 'w', format='NETCDF4', zlib=True)
    f.createDimension('time', time_data.shape[0])
    f.createDimension('vol', 32) 
    f.createDimension('diams', 14)
    f.createDimension('zscat', 40)
    # Volume
    tmpVar = f.createVariable('vol', 'f8', ('time','vol',))
    tmpVar[:,:] = vol
    # Diameters
    tmpVar = f.createVariable('diams', 'f8', ('time','diams',))
    tmpVar[:,:] = diams
    # Z-Scatter?
    tmpVar = f.createVariable('zscat', 'f8', ('zscat',))
    tmpVar[:] = zscat
    # Time
    tmpVar = f.createVariable('time', 'f8', ('time',))
    tmpVar[:] = time_data
    tmpVar.units = 'UTC'
    f.close()

def cmwpMatToNc(matlabDir) :
  """Aggregates data in original MATLAB files from the cmwp into deployment day 
  files saved as NetCDF files for each instrument.

  Dates and division of files are hardcoded.

  Writes one instrument at a time for memory usage purposes.
  """
  # cmwp-yyyymmdd-hhmmss.mat
  for inst in instruments :
    print 'Transferring '+inst
    for day in range(len(days)) :
      tmp = np.zeros(dataShapes[inst])
      times = calcFileTimes(day)
      #times = [datetime.datetime(2012, 5, 1, 15, 20, 00)]
      for time in times :
        matFile = 'cmwp-%s.mat' % (time.strftime('%Y%m%d-%H%M%S'))
        matFile = os.path.join(matlabDir, matFile)
        print 'Opening '+matFile
        # Need to load Matlab file dtype because some data is cast wrong
        data = scipy.io.loadmat(matFile, mat_dtype=True)
        timeKey = 'mlt_'+inst
        tmp = np.concatenate((tmp, 
                              np.concatenate((data[inst], data[timeKey]))),
                              axis=1)

      # Write NetCDF files
      tmp = tmp[:,1:] 
      ncFile = 'cmwp_day%d_%s.nc' % (day+1, inst)
      print 'Creating netCDF file '+ncFile
      f = netCDF4.Dataset(ncFile, 'w', format='NETCDF4', zlib=True)
      for i,v in enumerate(vars[inst]):
        f.createDimension(v, tmp.shape[1])
        tmpVar = f.createVariable(v, dataType[inst], (v,))
        tmpVar[:] = tmp[i,:]
        tmpVar.units = units[inst][i]
      f.createDimension('time', tmp.shape[1])
      tmpVar = f.createVariable('time', 'f8', ('time',))
      tmpVar[:] = tmp[-1,:]
      tmpVar.units = 'UTC'
      f.close()

#-------------------------------------------------------------------------------
# Classes
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Main
#-------------------------------------------------------------------------------
