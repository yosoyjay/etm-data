""" Collection of functions to deal with conversion of times from
MATLAB datenum, Python datetime, NumPy datetime64, and epoch in converting 
CMWP data from *.mat files to *.nc files.
"""
#--------------------------------------------------------------------------------
#Imports
#--------------------------------------------------------------------------------
import datetime
import pytz
import numpy as np

#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------
def epochToDatetime(epoch):
  """Converts UNIX epoch to python datetime
  """
  if isinstance(epoch, float):
    #return datetime.datetime.fromtimestamp(epoch, tz=pytz.utc)
    return datetime.datetime.fromtimestamp(epoch)
  
  if isinstance(epoch, np.ndarray):
    f = lambda x: datetime.datetime.fromtimestamp(x)
    vf = np.vectorize(f)
    return vf(epoch)

def datetime64ToEpoch(dt64):
  """Converts np.datetime64 to Unix epoch 

	Need to add 8 hours to convert from PST to UTC.
  """
  if isinstance(dt64, np.datetime64):
    if dt64.dtype == '<M8[ns]':
      return (dt64+np.timedelta64(8,'h')).astype('float64')/1e9
  if isinstance(dt64, np.ndarray):
    if dt64[0].dtype == '<M8[ns]':
      f = lambda x: int(x)/1e9+25200
      vf = np.vectorize(f)
      return vf(dt64)

  print 'Type not supported %s' % type(dt64)

def datenumToDatetime(datenum):
  """Converts MATLAB datenum to python datetime

  Required steps:
  1. datetime and datenum differ by 366 days
  """
  if isinstance(datenum, int) or isinstance(datenum, float):
    return (datetime.datetime.fromordinal(int(datenum)) +
            datetime.timedelta(days=datenum%1) -
            datetime.timedelta(days=366)) 

  if isinstance(datenum, np.ndarray):
    f = lambda x: (datetime.datetime.fromordinal(int(x)) +
                   datetime.timedelta(days=x%1) -
                   datetime.timedelta(days=366)) 
    vf = np.vectorize(f)
    return vf(datenum)

def datetimeToEpoch(dt):
  """ Converts datetime to UNIX epoch

  Must add 8 hours to this to get epoch seconds to work correctly.
	Obviously hardcoded to UTC/PST time differences.
  """
  if isinstance(dt, datetime.datetime):
    return (dt - datetime.datetime(1970, 1, 1) + 
            datetime.timedelta(hours=8)).total_seconds()

  if isinstance(dt, np.ndarray):
    f = lambda x: (x - datetime.datetime(1970, 1, 1) + 
                   datetime.timedelta(hours=8)).total_seconds()
                  
    vf = np.vectorize(f)
    return vf(dt)

  print 'Type not support %s' % type(dt)

def doyToDatetime(doy, year):
  """ Converts day of year to datetime format.
  """
  if isinstance(doy, float):
    return datetime.datetime(year,1,1) + datetime.timedelta(doy)

  if isinstance(doy, np.ndarray):
    f = lambda x: datetime.datetime(year,1,1) + datetime.timedelta(x)
    vf = np.vectorize(f)
    return vf(doy)
