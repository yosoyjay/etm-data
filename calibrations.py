""" Methods used to apply calibrations/corrections to the observed data.
All of these methods are to applied once and once only and are kept here
to document how they were applied.

These corrections were applied to data as part of the procA step.
"""
#-------------------------------------------------------------------------------
# Imports 
#-------------------------------------------------------------------------------
import os
import netCDF4

#-------------------------------------------------------------------------------
# Constants 
#-------------------------------------------------------------------------------
# WetLabs EcoPuck Constants from data sheets for these specific instruments.
# SN: BB2FVMG-154
# 'Blue' - 470 nm 
blueScaleFactor = 1.44e-05
blueDarkCounts = 39
# 'Red' - 700 nm - 
redScaleFactor = 2.94e-06
redDarkCounts = 45
# 'Cholorophyll'
chlScaleFactor = 0.0146
chlDarkCounts = 26

# WetLabs EcoPuck Constants from data sheets for these specific instruments.
# SN: FLNTURT-2830
# 'Cholorophyll'
f_chlScaleFactor = 0.0121
f_chlDarkCounts = 50
# 'NTU'
f_ntuScaleFactor = 0.0060
f_ntuDarkCounts = 51

#-------------------------------------------------------------------------------
# Constants 
#-------------------------------------------------------------------------------


def fixLocations(cruise_dir, cruise):
  """Fixes the location data in the raw netCDF files. Locations refer to 
	distinct anchorages during cruises.

  Parameters
  ----------
    cruise_dir - Directory that holds the data. 
    cruise - Cruise name ['spring', 'fall1', 'fall2']
  """
  if cruise == 'spring':
    from etm_data import spring_consts as consts
  elif cruise == 'fall1':
    from etm_data import fall_1_consts as consts
    lat = consts.lat
    lon = consts.lon
  elif cruise == 'fall2':
    from etm_data import fall_2_consts as consts
    lat = consts.lat
    lon = consts.lon
  else:
    print 'Invalid cruise id.'
    raise 

  # Done once, so if statements in loops is fine.
  # Not having Spring data split up by location prompted this.
  for root, _, files in os.walk(cruise_dir):
    for f in files:
      nc_file = os.path.join(root, f) 
      if '.nc' not in nc_file: 
        continue
      print 'Print appending file '+ nc_file
      d = netCDF4.Dataset(nc_file, 'a')
      v1 = d.variables['x']
      v2 = d.variables['y']
      if cruise == 'spring':
        if int(nc_file[-4]) > 2:
          v1[:] = consts.lon1
          v2[:] = consts.lat1
        else:
          v1[:] = consts.lon2
          v2[:] = consts.lat2
      else:
        v1[:] = lon
        v2[:] = lat
      d.close()

def applyEcoCorrection(cruise_dir):
  """Applies corrections to EcoPuck data as defined by the WetLabs instrument
  documentation.

  ! This must only be used one as part of data procesing.

  Parameters
  ----------
    cruise_dir - Directory with cruise data
  """  
  for root, _, files in os.walk(cruise_dir):
    for f in files:
      nc_file = os.path.join(root, f) 
      if 'Eco' not in nc_file:
        continue
      print 'Correcting data in '+nc_file
      d = netCDF4.Dataset(nc_file, 'a')
      v = d.variables['blue']
      v[:] = blueScaleFactor*(v[:] - blueDarkCounts)
      v.units = 'm^-1 sr^-1'
      v = d.variables['red']
      v[:] = redScaleFactor*(v[:] - redDarkCounts)
      v.units = 'm^-1 sr^-1'
      v = d.variables['cholorophyll']
      v[:] = chlScaleFactor*(v[:] - chlDarkCounts)
      v.units = 'mg/l'
      d.close()

def applyFLNTUCorrection(cruise_dir):
  """Applies corrections to FLNTU data as defined by the WetLabs instrument
  documentation.

  ! This must only be used one as part of data procesing.

  Parameters
  ----------
    cruise_dir - Directory with cruise data
  """  
  for root, _, files in os.walk(cruise_dir):
    for f in files:
      nc_file = os.path.join(root, f) 
      if 'FLNTU' not in nc_file:
        continue
      print 'Correcting data in '+nc_file
      d = netCDF4.Dataset(nc_file, 'a')
      v = d.variables['NTU']
      v[:] = f_ntuScaleFactor*(v[:] - f_ntuDarkCounts)
      v.units = 'NTU'
      v = d.variables['cholorophyll']
      v[:] = f_chlScaleFactor*(v[:] - f_chlDarkCounts)
      v.units = 'mg/l'
      d.close()

def applyMetadata(cruise_dir):
	"""Appends netCDF files with metadata about the cruise and the instruments
	that gathered the data.

	Parameters
	----------
		* cruise_dir - Directory with cruise data
	"""
	cruise = ['oc1210c', 'oc1210b']
	ship = 'R/V Oceanus'
  institute = 'CMOP'
	instrument = {'ACC1' : 'Shear probe',
			          'ACC2' : 'Shear probe',
								'ADV'  : 'Sontek ADV',
								'Altim' : 'Altimeter',
								'Comp' : 'Compass',
								'CTD' : 'Seabird',
								'Eco' : 'WetLabs EcoPuck SN: XXXXX',
								'LISST' : 'Sequoia LISST-100X SN: XXXXX',
								'Mopak' : 'Motionpak',
								'Oxygen' : 'Seabird',
								'uC' : 'Seabird',
								'uT' : 'Seabird',
								'VG1' : 'Shear probe',
								'VG2' : 'Shear probe',
								}

	for root, _, files in os.walk(cruise_dir):
		for f in files:
			nc_file = os.path.join(root, f)
			
